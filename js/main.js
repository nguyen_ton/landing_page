$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

});

gsap.fromTo(".header__bottom-left", { x: -300, opacity:0},{x: 0,opacity:1, duration: 2} );
gsap.fromTo(".header__bottom-right", {x: 300, opacity:0},{x: 0,opacity:1, duration: 2} );

gsap.fromTo(".header__bottom-link",{x:-300}, {duration:3, ease: "bounce.out",x:0})
gsap.fromTo(".section__about",{opacity:0, y:20}, {duration:1, opacity:1, y:0})

gsap.registerPlugin(ScrollTrigger);
gsap.to(".section1__title",{duration: 1,y:-10, opacity:1, scrollTrigger:".section1__link"})
gsap.to(".section1__sub",{duration: 1,y:-10, opacity:1, scrollTrigger:".section1__link"})
gsap.to(".section1__link",{duration: 1,y:-10, opacity:1, scrollTrigger:".section1__link"})


gsap.to(".section2__title",{duration: 2,y:-10, opacity:1, scrollTrigger:".section2__link"})
gsap.to(".section2__sub",{duration: 2,y:-10, opacity:1, scrollTrigger:".section2__link"})
gsap.to(".section2__link",{duration: 2,y:-10, opacity:1, scrollTrigger:".section2__link"})

gsap.to(".section3__title",{duration: 2,y:-10, opacity:1, scrollTrigger:".section3__link"})
gsap.to(".section3__sub",{duration: 2,y:-10, opacity:1, scrollTrigger:".section3__link"})
gsap.to(".section3__link",{duration: 2,y:-10, opacity:1, scrollTrigger:".section3__link"})

gsap.to(".section4__title",{duration: 2,y:-10, opacity:1, scrollTrigger:".section4__link"})
gsap.to(".section4__sub",{duration: 2,y:-10, opacity:1, scrollTrigger:".section4__link"})
gsap.to(".section4__link",{duration: 2,y:-10, opacity:1, scrollTrigger:".section4__link"})

